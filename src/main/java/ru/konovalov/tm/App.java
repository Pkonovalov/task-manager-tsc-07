package ru.konovalov.tm;

import ru.konovalov.tm.constants.ArgumentConst;
import ru.konovalov.tm.constants.TerminalConst;
import ru.konovalov.tm.model.Command;
import ru.konovalov.tm.util.NumberUtil;

import java.util.Scanner;

public class  App {

    public static void main(String[] args) {
        System.out.println("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                showAbout();
                break;
            case ArgumentConst.ARG_HELP:
                showHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                showVersion();
                break;
            case ArgumentConst.ARG_INFO:
                showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showSystemInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncorrectCommand();
        }
    }


    private static void showIncorrectArgument() {
        System.out.println("Error! Your argument is wrong! Try again it correctly!");
    }

    private static void showIncorrectCommand() {
        System.out.println("Error! Your command is wrong! Try again it correctly!");
    }

    private static void exit() {
        System.exit(0);
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }


    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Petr Konovalov");
        System.out.println("E -MAIL: pkonjob@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.ABOUT);
        System.out.println(Command.VERSION);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory (bytes): " + NumberUtil.formatBytes(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory (bytes): " + maxMemoryValue);
        final long memoryAvailable = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM (bytes): " + NumberUtil.formatBytes(memoryAvailable));
        final long usedMemory = memoryAvailable - freeMemory;
        System.out.println("User memory by JVM (bytes): " + NumberUtil.formatBytes(usedMemory));
        System.out.println("[OK]");
    }
}
